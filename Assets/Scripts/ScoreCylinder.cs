﻿using UnityEngine;
using System.Collections;

public class ScoreCylinder : ScoreObjectScript {


	public float reward = 15.0f;
	public GameObject sparks;
	// Use this for initialization
	void Start () {
		base.Start ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void takeAction (GameObject player , Collision collider)
	{
		if(this != null)
		if (this.gameObject.tag == collider.gameObject.tag) {
						Instantiate (sparks, player.transform.position, sparks.transform.rotation);
						PlayerScript playerScript = player.GetComponent<PlayerScript> ();
						playerScript.Score += reward;
						print (playerScript.Score);
						audio.Play ();
				}
	}

    public void OnDestory()
    {
        base.OnDestory();
    }


}
