﻿using UnityEngine;
using System.Collections;

public class PullBall : MonoBehaviour
{

    public float distance = 50.0f;
    public float pullLimit = -40.0f;
    public float speed = 1.0f;
    public GameObject ball;
    public float power = 2000.0f;

    private bool ready = false;
    private bool fire = false;
    private float moveCount = 0.0f;
    private Vector3 startPosition;
    private Vector3 panStart;
    private Vector3 delta;
    private Vector3 panEnd;

    void Start()
    {
        UserInput.OnPanBegin += MoveBegin;
        UserInput.OnPanHeld += MoveDownHeld;
        UserInput.OnPanEnd += MoveEnd;
        startPosition = transform.position;


    }


    void MoveBegin(Touch touch)
    {
        if (ready)
        {
            panStart = touch.position;
        }
    }

    private float deltaAmount = 0.0f;

    void MoveDownHeld(Touch touch)
    {
        if (ready && this != null)
        {
            delta = touch.deltaPosition;
            if (moveCount < distance && delta.y < 0.0f && deltaAmount > pullLimit)
            {
                transform.Translate(0.0f, delta.y * Time.deltaTime, 0.0f);
                moveCount += -delta.y * Time.deltaTime;

                fire = true;

                deltaAmount += delta.y;

            }
        }
    }

    void MoveEnd(Touch touch)
    {

        if (this != null)
        {
            if (ready)
            {
                if (fire && ready)
                {
                    ball.transform.TransformDirection(Vector3.forward * 10.0f);
                    ball.rigidbody.AddForce(0.0f, moveCount * power, 0.0f);
                    fire = false;
                    ready = false;
                }
                //Once we have reached the starting position fire off!
                transform.Translate(0.0f, 20.0f * Time.deltaTime, 0.0f);
               // moveCount -= 20.0f * Time.deltaTime;
                transform.position = startPosition;
                panEnd = touch.position;
                deltaAmount = 0.0f;
                moveCount = 0.0f;
            }
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ball")
        {
            ready = true;
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Ball")
        {
            ready = false;
        }
    }


    void Update()
    {
        if (moveCount <= 0.0f)
        {
            fire = false;
            moveCount = 0.0f;
        }
    }


    void OnDestory()
    {
        UserInput.OnPanBegin -= MoveBegin;
        UserInput.OnPanHeld -= MoveDownHeld;
        UserInput.OnPanEnd -= MoveEnd;
    }
}
