﻿using UnityEngine;
using System.Collections;

public class DestoryAfterTime : MonoBehaviour {

	public float seconds = 2.0f;

	// Use this for initialization
	void Start () {

		Destroy (this, seconds);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
