﻿using UnityEngine;
using System.Collections;

public class HighScoresMenuScript : BaseMenuState {
	
	// Use this for initialization

	public float offsetInX = 15.8f;
	public float offsetInY = 3.5f;
	float labelWidth = 200;
	float labelHeight = 30;
	
	void Awake()
	{
		base.Awake ();
	}
	
	void Start () {
		MenuStateManager.enterStateUsingName(	m_stateID);		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	
	public override void onGUI ()
	{
		float offsetX = transform.position.x+MenuConstants.OFFSET_X * offsetInX;
		float offsetY = transform.position.y+MenuConstants.OFFSET_Y * offsetInY;
		
		GUI.skin = guiSkin0;

		float heightChange = labelHeight;
		GUI.Label (new Rect (Screen.width / 2 - labelWidth,labelHeight, labelWidth, labelHeight), "High Score : " + PlayerPrefs.GetFloat("HighScore"));




		if(addButton(GUIHelper.screenRect (offsetX+.02f,offsetY+.4f,.4f,.1f), "Main Menu"))
		{
			Application.LoadLevel("MainMenu");
		}
	}
}
