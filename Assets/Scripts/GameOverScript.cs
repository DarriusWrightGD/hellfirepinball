﻿using UnityEngine;
using System.Collections;

public class GameOverScript : BaseMenuState
{

    // Use this for initialization

    public float offsetInX = 12.8f;
    public float offsetInY = 3.5f;
    float labelWidth = 200;
    float labelHeight = 30;
    public GameObject fireworks;
    void Awake()
    {
        base.Awake();
    }

    void Start()
    {

        PlayerScript.OnGameOver += GameOver;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GameOver()
    {
        if (this != null)
        {
            MenuStateManager.enterStateUsingName(m_stateID);

            if (PlayerPrefs.GetInt("Score") > 0 && PlayerPrefs.GetInt("Score") == PlayerPrefs.GetInt("HighScore"))
            {
                Instantiate(fireworks);
                audio.Play();
            }

            swapObjects(this.gameObject);
        }
    }

    public override void onGUI()
    {
        float offsetX = transform.position.x + MenuConstants.OFFSET_X * offsetInX;
        float offsetY = transform.position.y + MenuConstants.OFFSET_Y * offsetInY;

        GUI.skin = guiSkin0;

        float heightChange = labelHeight;
        GUI.Label(new Rect(Screen.width / 2 - labelWidth, Screen.height / 2 - labelHeight, labelWidth, labelHeight * 5.0f), "GameOver \n" + "High Score : " + PlayerPrefs.GetFloat("HighScore") + "\n" + "Your Score : " + PlayerPrefs.GetFloat("Score"));





        if (addButton(GUIHelper.screenRect(offsetX + .05f, offsetY + .4f, .25f, .1f), "Play Again"))
        {
            Application.LoadLevel("HellFirePinball");

        }

        if (addButton(GUIHelper.screenRect(offsetX + .05f, offsetY + .2f, .25f, .1f), "Main Menu"))
        {
            Application.LoadLevel("MainMenu");

        }
    }


    public void OnDestory()
    {
        PlayerScript.OnGameOver -= GameOver;
    }
}
