﻿using UnityEngine;
using System.Collections;

public class UserInput : MonoBehaviour
{


		public delegate void PanAction(Touch t);
		public delegate void TapAction (Touch t);
	public delegate void AndroidHardwareAction(KeyCode code);

		public static event TapAction OnTap;
		public static event TapAction OnTapHold;
		public static event TapAction OnTapRelease;
		
		public static event PanAction OnPanBegin;
		public static event PanAction OnPanHeld;
		public static event PanAction OnPanEnd;

	public static event AndroidHardwareAction OnAndroidInput;

		public float tapMaxMovement = 50.0f;
		private Vector2 movement;
		private bool tapGestureFailed = false;
		private Touch currentTouch;

		public float minPanTime = 0.4f;
		private bool panRecognized  =false;
		private float startTime;

		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void Update ()
		{


			if (Input.GetKey (KeyCode.Home)) 
		{
				if(OnAndroidInput != null)
				OnAndroidInput(KeyCode.Home);
				}
		if(Input.GetKey(KeyCode.Escape))
		{
			if(OnAndroidInput != null)
				OnAndroidInput(KeyCode.Escape);
		}


				if (Input.touchCount > 0) {
						Touch touch = Input.touches [0];
						if (OnTapHold != null) {
								OnTapHold (touch);
						}
						if (touch.phase == TouchPhase.Began) {
								movement = Vector2.zero;
								startTime = Time.time;
								
						} else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary) {
								movement += touch.deltaPosition;
								if (!panRecognized && Time.time - startTime > minPanTime) {
										panRecognized = true;
										tapGestureFailed = true;
										if (OnPanBegin != null) {
												OnPanBegin (touch);
										}
								} else if (panRecognized) {
										if (OnPanHeld != null)
												OnPanHeld (touch);
								} else if (movement.magnitude > tapMaxMovement)
										tapGestureFailed = true;

						}

		 else {
						if (!tapGestureFailed) {

								if (OnTap != null) {
										OnTap (touch);
								}

						}
					 	if(panRecognized)
						{
							if(OnPanEnd != null)
							{
						OnPanEnd(touch);
							}
						}
						tapGestureFailed = false;
				if (touch.phase == TouchPhase.Ended) {
					OnTapRelease (touch);
					
				}
			}
		}


	}

        void OnApplicationPause(bool pauseStatus)
        {
            Application.Quit();
        }
		

		void OnDestory ()
		{
				//OnTap = null;
		}
}
