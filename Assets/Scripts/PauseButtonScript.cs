﻿using UnityEngine;
using System.Collections;

public class PauseButtonScript : BaseMenuState {

	// Use this for initialization

	public GameObject pauseMenu;
	public GameObject gameManager;
	public float offsetInX = 15.8f;
	public float offsetInY = -3.5f;
	public delegate void PauseGameEvent(bool paused);
	public static event PauseGameEvent OnPauseEvent;

	void Awake()
	{
		base.Awake ();
	}

	void Start () {
		MenuStateManager.enterStateUsingName(	m_stateID);	
		UserInput.OnAndroidInput += AcceptInput;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void AcceptInput(KeyCode code)
	{
		if (KeyCode.Escape == code) {
			OnPauseEvent(true);
			swapObjects(pauseMenu);
				}
	}

	public override void onGUI ()
	{
		float offsetX = transform.position.x+MenuConstants.OFFSET_X * offsetInX;
		float offsetY = transform.position.y+MenuConstants.OFFSET_Y * offsetInY;
		
		GUI.skin = guiSkin0;

		if(addButton(GUIHelper.screenRect (offsetX+.05f,offsetY+.4f,.25f,.1f), "Pause"))
		{
			//gameManager.Paused = true;
			OnPauseEvent(true);
			swapObjects(pauseMenu);
		}
	}
}
