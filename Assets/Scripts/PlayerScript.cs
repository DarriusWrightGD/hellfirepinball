﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{


    Vector3 force = new Vector3(0, 0, 0);
    Vector3 startPosition;
    Vector3 direction;
    Vector3 lastDirection;
    bool paused;
    bool gameOver = false;
    public int lives = 3;

    public delegate void CollisionEnterEvent(GameObject player, Collision collider);
    public delegate void CollisionExitEvent(GameObject g, Collision collider);
    public delegate void GameOverEvent();

    public static event CollisionEnterEvent OnCollisionExitEvent;
    public static event CollisionExitEvent OnCollisionEnterEvent;
    public static event GameOverEvent OnGameOver;


    public void SetPaused(bool paused)
    {
        if (this != null)
        {
            this.paused = paused;
            rigidbody.isKinematic = paused;
        }
    }

    public void AcceptInput(KeyCode code)
    {
        if (code == KeyCode.Home)
        {
            Application.LoadLevel("MainMenu");
        }
    }

    public AudioClip paddleCollision;
    // Use this for initialization
    void Start()
    {
        startPosition = transform.position;
        startPosition.y += 2.0f;
        PauseMenuScript.OnUnPauseEvent += SetPaused;
        PauseButtonScript.OnPauseEvent += SetPaused;
    }

    // Update is called once per frame
    void Update()
    {
        if (!paused && !gameOver)
        {
            if (transform.position.y < -8.0f && Input.GetMouseButton(0))
            {
                lives--;
                if (lives <= 0)
                {
                    float lastScore = PlayerPrefs.GetFloat("HighScore");
                    print(lastScore);
                    if (lastScore < Score)
                    {
                        PlayerPrefs.SetFloat("HighScore", Score);
                    }
                    PlayerPrefs.SetFloat("Score", Score);
                    PlayerPrefs.Save();

                    OnGameOver();
                }

                rigidbody.velocity = Vector3.zero;
                rigidbody.angularVelocity = Vector3.zero;
                transform.position = startPosition;

               
            }
        }


    }


    public void GameOver()
    {
        gameOver = true;
    }

    public float Score
    {
        get;
        set;
    }

    void FixedUpdate()
    {
        if (!paused)
        {
            rigidbody.AddForce(force);
            //force = new Vector3 (0, 0, 0);
        }
    }


    void OnCollisionEnter(Collision collision)
    {
        if (OnCollisionEnterEvent != null)
            OnCollisionEnterEvent(this.gameObject, collision);


    }

    void OnCollisionExit(Collision collision)
    {
        if (OnCollisionExitEvent != null)
            OnCollisionExitEvent(this.gameObject, collision);
    }


    public void OnGUI()
    {
        GUI.Label(new Rect(Screen.width / 10.0f, Screen.height / 20.0f, 200, 200), "Score : " + Score + "\nLives Left : " + lives);
    }

    public void AddForce(Vector3 f)
    {
        this.force += f;
    }


    public void OnDestory()
    {
        PauseMenuScript.OnUnPauseEvent -= SetPaused;
        PauseButtonScript.OnPauseEvent -= SetPaused;
        PlayerScript.OnGameOver -= GameOver;
    }
}
