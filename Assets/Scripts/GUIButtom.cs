﻿using UnityEngine;
using System.Collections;

public class GUIButton : MonoBehaviour
{

   public delegate void ButtonPress();
    public event ButtonPress OnButtonPress;

    // Use this for initialization
    void Start()
    {
        UserInput.OnTap += OnTouch;
    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnTouch(Touch touch)
    {
        if(guiTexture.HitTest(touch.position))
        {
            if(OnButtonPress != null)
                OnButtonPress();
        }
    }
}
