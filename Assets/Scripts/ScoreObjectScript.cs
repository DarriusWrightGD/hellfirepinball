﻿using UnityEngine;
using System.Collections;

public abstract class ScoreObjectScript : MonoBehaviour {

	// Use this for initialization
	public void Start () {
		PlayerScript.OnCollisionEnterEvent += takeAction;
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public abstract void takeAction(GameObject gameObject, Collision collider);

    public void OnDestory()
    {
        PlayerScript.OnCollisionEnterEvent -= takeAction;

    }

}
