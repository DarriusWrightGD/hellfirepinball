﻿using UnityEngine;
using System.Collections;

public class PaddleScript : MonoBehaviour
{

    public enum Side
    {
        Left,
        Right
    }
		;


    public Side ScreenSide = Side.Left;
    public float restPosition = 0.0f;
    public float pressedPosition = -45.0f;
    public float paddleStrength = 10.0f;
    public float paddleDamper = 1.0f;
    private float position;
    JointSpring spring = new JointSpring();
    bool touched = false;
    void Start()
    {
        position = restPosition;
        UserInput.OnTapHold += UserHold;
        UserInput.OnTapRelease += UserRelease;
        JointLimits limits = new JointLimits();
        limits.minBounce = 0.02f;
        limits.maxBounce = 0.02f;
        limits.min = restPosition;
        limits.max = pressedPosition;
        hingeJoint.limits = limits;
        hingeJoint.useLimits = true;



    }
    void Update()
    {

        JointSpring spring = new JointSpring();

        spring.targetPosition = pressedPosition;
        spring.spring = paddleStrength;
        spring.targetPosition = position;
        spring.damper = paddleDamper;
        JointLimits limits = new JointLimits();
        limits.minBounce = 0.02f;
        limits.maxBounce = 0.02f;
        limits.min = restPosition;
        limits.max = pressedPosition;
        hingeJoint.limits = limits;
        hingeJoint.useLimits = true;

        hingeJoint.spring = spring;
    }

    void UserHold(Touch touch)
    {
        if (isTouchOnSide(touch))
        {
            position = pressedPosition;
        }
    }

    void UserRelease(Touch touch)
    {
        position = restPosition;
        print("released");
    }
    public bool isTouchOnSide(Touch t)
    {
        return (Side.Left == ScreenSide && t.position.x < Screen.width / 2) || (Side.Right == ScreenSide && t.position.x > Screen.width / 2);
    }

    public bool isMouseOnSide()
    {
        return (Side.Left == ScreenSide && Input.mousePosition.x < Screen.width / 2) || (Side.Right == ScreenSide && Input.mousePosition.x > Screen.width / 2);
    }

    /*
    float paddlePower = 5000.0f;
    public float rotationSpeed = 2.0f;
    public float upRotation = 45.0f;
    public float downRotation = 315.0f;
    public float side = 0;
    bool rotateUp = false;
    bool rotateDown = false;
    bool holding = false;
    float degreesRotated;
    public Side ScreenSide = Side.Left;
    public AudioClip paddleCollision;
    bool canHitBall = false;
    public GameObject player;
	
    public bool isPaddleUp {
            get{ return rotateUp;}
    }

    Vector3 collisionNormal;
    // Use this for initialization
    void Start ()
    {
            transform.rotation = Quaternion.Euler (new Vector3 (0, 0, downRotation));
            PlayerScript.OnCollisionEnterEvent += CollisionWithPlayer;
            PlayerScript.OnCollisionExitEvent += CollisionExitWithPlayer;
    }

    public void CollisionExitWithPlayer (GameObject g, Collision collider)
    {
            if (collider.gameObject.tag == gameObject.tag) {
			
                    canHitBall = false;
                    collisionNormal = new Vector3(0,0,0);
            }

    }

    public void CollisionWithPlayer (GameObject  g, Collision collider)
    {
            if (collider.gameObject.tag == gameObject.tag) {
                    PaddleScript script = collider.transform.parent.GetComponent<PaddleScript> ();
                    if (script.ScreenSide == ScreenSide) {
                            canHitBall = true;
                            collisionNormal = collider.contacts [0].normal;
                    }		
						
            }
    }

    // Update is called once per frame
    void Update ()
    {

            if (Input.GetMouseButtonDown (0) && rotateUp) {
                    rotateDown = false;
                    holding = true;
            }

            if (Input.GetMouseButtonUp (0)) {
                    if (holding)
                            rotateDown = true;
                    holding = false;

            }

            if (Input.GetMouseButton (0) && isMouseOnSide () && !rotateDown && !rotateUp && !holding) {
                    rotateUp = true;
            }



            if (rotateDown && !rotateUp) {

                    if (degreesRotated >= 0.0f) {
                            degreesRotated += Mathf.Abs (upRotation) * rotationSpeed * Time.deltaTime;
		
                            transform.RotateAround (transform.position, new Vector3 (0, 0, 1), downRotation * rotationSpeed * Time.deltaTime);
                            //paddle.transform.Rotate(new Vector3(0,0,-upRotation * rotationSpeed * Time.deltaTime));		
			
                            if (degreesRotated >= 90.0f) {
                                    transform.rotation = Quaternion.Euler (new Vector3 (0, 0, downRotation));		
                                    rotateDown = false;
                                    degreesRotated = 0.0f;
				
                            }
                    }
            }

            if (rotateUp && !rotateDown) {
                    if (degreesRotated >= 0.0f) {
                            //paddle.transform.RotateAround(transform.position,new Vector3(0,0,1),upRotation * rotationSpeed * Time.deltaTime);
                            degreesRotated += Mathf.Abs (upRotation) * rotationSpeed * Time.deltaTime;

			
                            transform.Rotate (new Vector3 (0, 0, upRotation * rotationSpeed * Time.deltaTime));		
			
                            if (degreesRotated >= 90.0f) {
                                    transform.rotation = Quaternion.Euler (new Vector3 (0, 0, upRotation));
				
				
                                    rotateUp = false;
                                    rotateDown = true;
                                    degreesRotated = 0.0f;
                                    //bool down = Input.GetMouseButtonDown (0);
                                    if (Input.GetMouseButton (0)) {
                                            rotateDown = false;
                                            holding = true;
                                    }
                            }
                    }
            }
	
            if (isPaddleUp && canHitBall) {

                //	player.GetComponent<PlayerScript>().AddForce (collisionNormal * 10.0f);
        //print (collisionNormal);
        canHitBall = false;
                    audio.PlayOneShot (paddleCollision);
			
            }
    }

    public bool isMouseOnSide ()
    {
            return (Side.Left == ScreenSide && Input.mousePosition.x < Screen.width / 2) || (Side.Right == ScreenSide && Input.mousePosition.x > Screen.width / 2); 
    }

*/
}
