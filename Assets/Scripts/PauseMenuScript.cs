﻿using UnityEngine;
using System.Collections;

public class PauseMenuScript : BaseMenuState {


	public GameObject pauseButton;
	public GameObject gameManager;
	public delegate void UnPauseGameEvent(bool paused);
	public static event UnPauseGameEvent OnUnPauseEvent;

	// Use this for initialization
	void Start () {
	
	}


void  OnDestory()
    {

    }
	
	// Update is called once per frame
	void Update () {
        UserInput.OnAndroidInput += AcceptInput;
	}


    public void AcceptInput(KeyCode code)
    {
        if (KeyCode.Escape == code)
        {
            if (OnUnPauseEvent != null)
                OnUnPauseEvent(true);
            if(this != null)
                swapObjects(this.gameObject);
        }
    }
    public override void onGUI ()
	{
		float offsetX = transform.position.x+MenuConstants.OFFSET_X * 5;
		float offsetY = transform.position.y+MenuConstants.OFFSET_Y;
		
		GUI.skin = guiSkin0;




        if (GUI.Button(GUIHelper.screenRect(offsetX + .05f, offsetY + .275f, .35f, .1f), "Play"))//addButton (GUIHelper.screenRect (offsetX+.05f,offsetY+.275f,.35f,.1f), "Play"))
		{
			//gameManager.Paused = false;
			//swapObjects(pauseButton);
            swapObjects(null);
			if(OnUnPauseEvent != null)
			OnUnPauseEvent(false);
		}

        //if (addButton(GUIHelper.screenRect(offsetX + .05f, offsetY + .4f, .35f, .1f), "Main Menu"))
        if (GUI.Button(GUIHelper.screenRect(offsetX + .05f, offsetY + .4f, .35f, .1f), "Main Menu"))
		{
			Application.LoadLevel("MainMenu");
		}		
	}
}
