﻿using UnityEngine;
using System.Collections;

public class MainMenuScript : BaseMenuState {

	//public GameObject pauseMenu;
	public GameObject highScoresMenu;
	public float regularTimeScale = 1f;

	void Awake()
	{
		base.Awake ();
	}

	// Use this for initialization
	void Start () {

		MenuStateManager.enterStateUsingName(	m_stateID);		
	
	}
	
	public void Update()	
	{

	}

	public override void onGUI()
	{

		float offsetX = transform.position.x+MenuConstants.OFFSET_X * 5;
		float offsetY = transform.position.y+MenuConstants.OFFSET_Y;

		GUI.skin = guiSkin0;

        float heightChange = 30.0f;
        int labelWidth = 200;
        int labelHeight = 60;
        GUI.Label(new Rect(Screen.width / 2 - labelWidth, labelHeight, labelWidth, labelHeight), "Darrius Wright's\nHellFire Pinball");


		if(addButton (GUIHelper.screenRect (offsetX+.05f,offsetY+.275f,.35f,.1f), "Play"))
		{
			Application.LoadLevel("HellFirePinball");
		}

		if(addButton(GUIHelper.screenRect (offsetX+.05f,offsetY+.4f,.35f,.1f), "HighScores"))
		{
			Application.LoadLevel("HighScores");
		}

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

	}
}
